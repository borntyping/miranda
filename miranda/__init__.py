"""Miranda: an IRC bot"""

from __future__ import absolute_import, print_function, unicode_literals

import argparse
import os
import json
import logging

import six

from miranda.console import Console
from miranda.dispatch import listener, Listener, Namespace, Dispatch
from miranda.irc import IRC
from miranda.reactor import Reactor
from miranda.utils import tree

__all__ = ['listener', 'Listener', 'Namespace', 'Miranda']
__author__ = "Sam Clements <sam@borntyping.co.uk>"
__version__ = "0.1.0"

if six.PY3:
    import urllib.parse as urlparse
else:
    import urlparse

class Miranda(object):
    """Ties the various objects together into a single interface"""

    CONNECTION_TYPES = [IRC, Console]

    def __init__(self):
        self.config = dict()
        self.config.setdefault('paths', [])
        self.config.setdefault('modules', [])
        self.config.setdefault('connections', [])
        self._init = False

    def load_file(self, filename):
        with open(os.path.expanduser(filename)) as file:
            self.config.update(json.loads(file.read()))

    def load_args(self, args):
        self.config['connections'].extend(args.connections)
            
        if args.console:
            self.config['connections'].append({
                'type': 'console',
                'miranda': self,
            })

        self.config['debug'] = args.debug
        self.config['dry_run'] = args.dry_run

    def init_connections(self, connection_configuration, default='irc'):
        types = dict()
        for type in self.CONNECTION_TYPES:
            types[type.__name__.lower()] = type

        connections = []
        for config in connection_configuration:
            # Parse urls into a useful format
            if isinstance(config, six.string_types):
                config = Miranda.config_from_url(config)

            # Create an instance using the connection configuration
            type_name = config.pop('type', default).lower()
            connections.append(types[type_name](**config))
        
        return connections

    @staticmethod
    def config_from_url(url):
        """Builds a dictionary of kwargs from an url"""
        components = urlparse.urlparse(url, allow_fragments=False)

        config = {}

        def consider(key, value):
            if value: config[key] = value

        consider('type', components.scheme)
        consider('username', components.username)
        consider('password', components.password)
        consider('host', components.path)
        consider('host', components.hostname)
            
        if components.port:
            config['port'] = int(components.port)
            
        return config

    @staticmethod
    def expand_paths(paths):
        return [os.path.expanduser(path) for path in paths]

    def init(self, start=True, dump=False):
        # Pop the core configuration and make it usable
        paths = self.expand_paths(self.config.pop('paths'))
        modules = self.expand_paths(self.config.pop('modules'))
        
        self.dispatch = Dispatch(paths, self.config.get('use_builtins', True))
        self.dispatch.load_modules(*modules)

        connections = self.init_connections(self.config.pop('connections'))
        self.reactor = Reactor(connections, self.dispatch)

        if dump:
            print(tree("Config", children=[self.config]))
            print(tree(self.dispatch, func=repr))
            print(tree(self.reactor, func=repr))

        if start:
            self.reactor.go()
        
        return self

parser = argparse.ArgumentParser(prog="miranda")

parser.add_argument('filename', nargs='?', default='~/.miranda/config.json',
    help="the config file to attempt to load")
    
parser.add_argument('-c', '--connect',
    action='append', metavar='HOST:PORT', dest='connections', default=[],
    help="an irc server to connect to (host:port)")

parser.add_argument('-C', '--console', action='store_true',
    help="start a Console connection")
    
parser.add_argument('-n', '--dry-run', action='store_true',
    help="halt before connecting")
    
parser.add_argument('-d', '--debug', action='store_true',
    help="print extra debug information")

def main():
    """Parses command line options to build and run a Miranda instance"""
    args = parser.parse_args()
    miranda = Miranda()
    miranda.load_file(args.filename)
    miranda.load_args(args)
    miranda.init(start=(not args.dry_run), dump=args.debug)
