from __future__ import absolute_import, print_function, unicode_literals

class Event(object):
    pass

class NullEvent(Event):
    pass

class Message(Event):
    def reply(self, message):
        raise NotImplementedError("Subclass has not defined reply()")
