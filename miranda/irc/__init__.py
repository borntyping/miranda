"""The Connection class, which handles a single IRC connection"""

from __future__ import absolute_import, print_function, unicode_literals

import socket

import six

from miranda.exceptions import Disconnected, PermanentlyDisconnected
from miranda.irc.events import IRCEvent
from miranda.irc.parser import format_line
from miranda.reactor import EventHandler
from miranda.utils import short_repr, quick_repr

class IRC(EventHandler):
    ENCODING = 'utf-8'
    NEWLINE = bytes("\r\n", ENCODING) if six.PY3 else str("\r\n")
    
    MAX_RECONNECTS = 1

    fd = None
    socket = None

    def __init__(self, host, port=None, username=None, password=None):
        self.host = host
        self.port = port or 6667
        self.username = username or 'miranda'
        self.password = password
        self.reconnect_count = 0

    def __repr__(self):
        return quick_repr(self, {
            'host': self.host,
            'port': self.port,
            'username': self.username,
            'password': self.password,
        })

    def connect(self):
        self._buffer = six.binary_type()
        self._linebuffer = list()

        self.socket = socket.socket()
        self.socket.setsockopt(socket.IPPROTO_TCP, socket.TCP_NODELAY, 1)
        self.socket.connect((self.host, self.port))

        self.fd = self.socket.fileno()

        print("Connected to {0}:{1}".format(self.address, self.port))

        #self.write('NICK :miranda')
        #self.write('USER miranda * * :miranda')
        self.write('QUIT :Disconnected because this is a test')

    def connection_completed(self):
        """This is intended to be called by a plugin, so that a successful
        protocol level connection can be acknowledged."""
        self.reconnect_count = 0

    def read(self, recv_size=1024):
        self._buffer += self.socket.recv(recv_size)

        while self.NEWLINE in self._buffer:
            line, self._buffer = self._buffer.split(self.NEWLINE, 1)
            self._linebuffer.append(line.decode(self.ENCODING))

        while self._linebuffer:
            line = self._linebuffer.pop(0)

            if line.startswith('PING :'):
                self.write('PONG :' + line[6:])
                break

            if line.startswith('ERROR :'):
                raise Disconnected("Server error: " + line[7:])

            event = IRCEvent.parse(line, connection=self)

            if event is not None:
                yield event

        raise StopIteration

    def _write(self, line):
        """Encode and send a single line"""
        self.socket.send(line.encode(self.ENCODING) + self.NEWLINE)

    def write(self, command, parameters=None, trailing=None):
        """Format an IRC message and send it"""
        self._write(format_line(None, command, parameters, trailing))

    def __try(self, func, *args, **kwargs):
        """Attempt to call a function, but pass on socket errors"""
        try:
            return func(*args, **kwargs)
        except socket.error:
            pass

    def disconnect(self, exception=None, message="Disconnected"):
        if exception and not message:
            message = str(exception)
        
        if self.socket is not None:
            self.__try(self.write, "QUIT :" + message)
            self.__try(self.socket.shutdown, socket.SHUT_RDWR)
            self.__try(self.socket.close)
            del self.socket

    def reconnect(self):
        """Attempt to reconnect to the server"""
        self.disconnect("Reconnecting")
        self.reconnect_count += 1
        if self.reconnect_count >= self.MAX_RECONNECTS:
            raise PermanentlyDisconnected("Reached reconnect limit")
        self.connect()
