from __future__ import absolute_import, print_function, unicode_literals

from miranda.events import Event, Message
from miranda.irc.parser import format_line, parse_line
from miranda.utils import EqualityMixin

class IRCEvent(Event, EqualityMixin):
    classes = dict()

    @classmethod
    def _register(cls, name, obj):
        cls.classes[str(name).lower()] = obj

    @classmethod
    def register(cls, obj):
        cls._register(obj.__name__, obj)
        return obj

    @classmethod
    def register_numeric(cls, code):
        def decorator(obj):
            cls._register(code, obj)
            return obj
        return decorator

    @classmethod
    def parse(cls, line, connection=None):
        source, command, parameters, trailing = parse_line(line)

        if command.lower() in cls.classes:
            cls = cls.classes[command.lower()]

        return cls(
            connection=connection,
            source=source,
            command=command,
            parameters=parameters,
            trailing=trailing)

    def __init__(self, connection, source, command, parameters, trailing):
        self.connection = connection
        self.source = source
        self.command = command
        self.parameters = parameters
        self.trailing = trailing

        # Subclasses can define a function that is used to perform
        # extra setup with the existing attributes
        if hasattr(self, 'init') and callable(self.init):
            self.init()

    def __repr__(self):
        return ":{0!r} {1!r} {2!r} :{3!r}".format(*self.to_tuple())

    def __str__(self):
        return format_line(*self.to_tuple())

    def to_tuple(self):
        return self.source, self.command, self.parameters, self.trailing

    @property
    def sender(self):
        return self.source[0]

    @property
    def me(self):
        if hasattr(self.connection, 'nick'):
            return self.connection.nick
        return None

class IRCChannelEvent(IRCEvent):
    def init(self):
        # The first parameter in a channel message is always the receiver
        self.receiver = self.parameters.pop(0)

        # If the message has been sent directly to me, the sender can be
        # found in the source and the message is 'private'
        if self.receiver == self.me:
            self.channel = self.sender
            self.private = True
        # Otherwise, the message was sent to a channel I am in
        else:
            self.channel = self.receiver
            self.private = False

"""4.1 Connection Registration"""

@IRCEvent.register
class Quit(IRCEvent):
    reason = property(lambda self: self.trailing)

@IRCEvent.register
class Join(IRCEvent):
    channel = property(lambda self: self.parameters[0])

@IRCEvent.register
class Part(IRCEvent):
    channel = property(lambda self: self.parameters[0])

@IRCEvent.register
class Topic(IRCEvent):
    channel = property(lambda self: self.parameters[0])

"""4.4 Sending messages"""

@IRCEvent.register
class Privmsg(IRCChannelEvent, Message):
    text = property(lambda self: self.trailing)
    
    def reply(self, message):
        pass

@IRCEvent.register
class Notice(IRCChannelEvent):
    def reply(self):
        raise Exception("Don't reply to IRC NOTICE messages")

"""6.2 Command responses"""

@IRCEvent.register_numeric(331)
def no_topic(**kwargs):
    """Return a Topic event with an empty topic"""
    kwargs['trailing'] = ""
    return Topic(**kwargs)

@IRCEvent.register_numeric(332)
class Topic(IRCEvent):
    topic = property(lambda self: self.trailing)

@IRCEvent.register_numeric(333)
class TopicMeta(IRCEvent):
    channel = property(lambda self: self.parameters[1])
    setter = property(lambda self: self.parameters[2])
    time = property(lambda self: self.parameters[3])

@IRCEvent.register_numeric(375)
def motd_start(connection, **kwargs):
    connection.motd = ""

@IRCEvent.register_numeric(372)
def motd(connection, trailing, **kwargs):
    connection.motd += trailing[2:] + "\n"

@IRCEvent.register_numeric(376)
class MOTD(IRCEvent):
    text = property(lambda self: self.connection.motd)
    
    def init(self):
        """Strip the trailing newline from the MOTD"""
        self.connection.motd = self.connection.motd[:-1]
