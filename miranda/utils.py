# coding: utf8

from __future__ import absolute_import, print_function, unicode_literals

def short_repr(obj, data=None):
    """A shortcut function for writing __repr__ functions"""
    
    return "<{module}.{cls}{data}>".format(
        module=obj.__module__, cls=obj.__class__.__name__,
        data=' {0}'.format(data) if data is not None else '')

#def tree_repr(obj, children=None, depth=0):
    #depth = depth + 1
    #string = "<{module}.{cls}>".format(
        #module=obj.__module__, cls=obj.__class__.__name__)
    #if children is not None:
        #for child in children:
            #if hasattr(child, '__tree__'):
                #child_string = child.__tree__(depth=depth)
            #else:
                #child_string = child.__str__()
            #string += "\n" + (' ' * depth * 2) + child_string
    #return string

def quick_repr(obj, attributes=None):
    attributes = [name + '=' + str(item) for name, item in attributes.items()]
    return obj.__class__.__name__ + '(' + ', '.join(attributes) + ')'

def tree(obj, depth=0, step=2, prompt='', fill=' ', func=str, children=None):
    """Return a string describing an object in tree form"""
    string = prompt + func(obj)
    depth += 1

    if children is None and hasattr(obj, '__children__'):
        children = obj.__children__()
        
    if children is not None:    
        for child in children:
            string += "\n" + (fill * depth * step)
            string += tree(child,
                depth=depth, step=step, prompt=prompt, fill=fill, func=func)
            
    return string

class EqualityMixin(object):
    """Allows attribute-based comparison between two instances of a class"""

    def __eq__(self, other):
        if isinstance(other, self.__class__):
            return self.__dict__ == other.__dict__
        return False

    def __ne__(self, other):
        return not self.__eq__(other)
