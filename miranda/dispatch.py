"""Dispatch classes and related functionality"""

from __future__ import absolute_import, print_function, unicode_literals

import collections
import functools
import imp
import inspect
import os

import six

from miranda.utils import quick_repr

def listener(event_class, *args, **kwargs):
    """Turn a function into a Listener object"""
    def decorator(function):
        return Listener(function, event_class, *args, **kwargs)
    return decorator

class Listener(object):
    def __init__(self, function, event_class, *args, **kwargs):
        functools.update_wrapper(self, function)
        self.function = function
        self.event_class = event_class

    def __repr__(self):
        return quick_repr(self, {
            'class': self.event_class.__name__,
            'function': self.function.__name__,
        })

    def __call__(self, event):
        if isinstance(event, self.event_class):
            self.function(event)

class Namespace(list):
    """A collection of listeners

    Acts as a {type(Event): [listener, ...]} dictionary."""

    def __call__(self, event):
        """Dispatch the event to every listener under the event's class"""
        for listener in self:
            listener(event)

    def __repr__(self):
        return quick_repr(self, {
            'listeners': [l.__name__ for l in self],
        })

    def __children__(self):
        return self

    def __tree__(self, depth=0):
        return tree_repr(self, self, depth=depth)
    
    def unload_module(self, module_name):
        """Removes listeners from the namespace which came from the module"""
        for listener in self:
            if listener.__module__ == module_name:
                self.remove(listener)

    def listener(self, event_class, *args, **kwargs):
        """Similar to @listener, but adds to this namespace

        Does not modify the function, unlike @listener."""
        def decorator(function):
            self.append(Listener(function, event_class, *args, **kwargs))
            return function
        return decorator

class Dispatch(collections.defaultdict):
    """A dispatcher which loads listeners from modules into namespaces.

    Modules are loaded from a list of paths, and don't conflict with existing
    python modules. Acts as a `{name: Namespace}` dictionary."""

    BUILTIN_DIR = os.path.join(os.path.dirname(__file__), "modules")
    
    def __init__(self, paths, use_builtins=True, factory=Namespace):
        super(Dispatch, self).__init__(factory)
        self.paths = paths

        if use_builtins:
            self.paths.append(self.BUILTIN_DIR)

    def __call__(self, event):
        """Dispatch the event to each namespace"""
        for namespace in self.values():
            namespace(event)

    def __repr__(self):
        return quick_repr(self, {
            'modules': self.keys(),
        })

    def __children__(self):
        return self.values()

    def import_module(self, name):
        """Import a python module from the loader's path and return it"""
        fp, path, description = imp.find_module(name, self.paths)
        return imp.load_module(name, fp, path, description)

    def load_modules(self, *modules):
        """Load multiple modules"""
        for module in modules:
            self.load_module(module)

    def load_module(self, module_name):
        """Load listeners and namespaces from a module"""
        module = self.import_module(module_name)
        module_name = six.text_type(module.__name__)

        for name, obj in inspect.getmembers(module):
            if isinstance(obj, Namespace):
                self[module_name + '.' + name] = obj
            elif isinstance(obj, Listener):
                self[module_name].append(obj)
        self._clean_namespaces()

    def unload_module(self, module_name):
        """Remove all listeners from the given module"""
        for namespace in self.values():
            namespace.unload_module(module_name)
        self._clean_namespaces()

    def reload_module(self, name):
        """Unloads and loads a module

        WARNING: Reloading a module is not always completely clean,
        and variables from the existing module may still be available."""
        self.unload_module(name)
        self.load_module(name)

    def _clean_namespaces(self):
        """Remove empty namespaces"""
        for namespace_name, namespace in list(self.items()):
            if not namespace:
                del self[namespace_name]
