"""Exception classes"""

from __future__ import absolute_import, print_function, unicode_literals

class Disconnected(Exception):
    """The handler has disconnected"""
    pass

class PermanentlyDisconnected(Exception):
    """The handler has disconnected and should not be reconnected"""
    pass
