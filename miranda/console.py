# coding=utf8

from __future__ import absolute_import, print_function, unicode_literals

import readline
import sys

from miranda.events import Event
from miranda.reactor import EventHandler
from miranda.utils import quick_repr

class ConsoleEvent(Event):
    def __init__(self, console):
        self.console = console

class ConsoleInput(ConsoleEvent):
    def __init__(self, console, text):
        super(ConsoleInput, self).__init__(console)
        self.text = text

class Console(EventHandler):
    def __init__(self, miranda=None):
        self.miranda = miranda

    def __repr__(self):
        return quick_repr(self, {})
        
    def connect(self, stdin=sys.stdin, stdout=sys.stdout):
        self.stdin = stdin
        self.stdout = stdout
        self.fd = self.stdin.fileno()

        self.write("Started miranda console")
        self.prompt()

    def read(self):
        yield ConsoleInput(self, self.stdin.readline().strip())

    def _write(self, *args, **kwargs):
        print(*args, file=self.stdout, **kwargs)
        self.stdout.flush()

    def write(self, output, *args, **kwargs):
        self._write(output, *args, **kwargs)

    def prompt(self):
        self._write("❯", readline.get_line_buffer(), end='')

    def disconnect(self, exception=None):
        self.stdin.close()
        self.stdout.close()
        
        # Never allow the reactor to try and reconnect
        raise PermanentlyDisconnected
