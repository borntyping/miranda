from __future__ import absolute_import, print_function, unicode_literals

import py.test

from miranda import Miranda

import urlparse

@py.test.mark.parametrize(('url', 'expected'), (
    ('irc://user:pass@irc.example.com:6667', {
        'type': 'irc',
        'username': 'user',
        'password': 'pass',
        'host': 'irc.example.com',
        'port': 6667,
    }),
    ('irc.example.com', {
        'host': 'irc.example.com',
    }),
    ('irc://irc.example.com:6667', {
        'type': 'irc',
        'host': 'irc.example.com',
        'port': 6667
    }),
))
def test_make_config(url, expected):
    assert Miranda.config_from_url(url) == expected
