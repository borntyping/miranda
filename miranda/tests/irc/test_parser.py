from __future__ import absolute_import, print_function, unicode_literals

import py.test

from miranda.irc.events import IRCEvent
from miranda.irc.parser import parse_line, format_line

test_data = [
    (":aleph!user@example.com NICK miranda",
        (("aleph", "user", "example.com"), "NICK", ["miranda"], None)),
    (":miranda!user@example.com QUIT :Goodbye",
        (("miranda", "user", "example.com"), "QUIT", [], "Goodbye")),
]

@py.test.mark.parametrize(('line', 'parsed'), test_data)
def test_parse_line(line, parsed):
    assert parse_line(line) == parsed

@py.test.mark.parametrize(('line', 'parsed'), test_data)
def test_format_line(line, parsed):
    assert format_line(*parsed) == line

@py.test.mark.parametrize(('line', 'parsed'), test_data)
def test_event_as_str(line, parsed):
    assert str(IRCEvent(None, *parsed)) == line

class MockIRC(object):
    stream = """
        :irc.example.com NOTICE AUTH :*** Looking up your hostname...
        :irc.example.com NOTICE AUTH :*** Found your hostname (example.com)
        :irc.example.com 001 miranda :Welcome to the AFNet IRC Network miranda!bot@example.com
        :irc.example.com 002 miranda :Your host is irc.example.com, running version Unreal3.2.8
        :irc.example.com 003 miranda :This server was created Tue Mar 10 2009 at 23:47:42 GMT
        :irc.example.com 004 miranda irc.example.com Unreal3.2.8 iowghraAsORTVSxNCWqBzvdHtGp lvhopsmntikrRcaqOALQbSeIKVfMCuzNTGj
        :irc.example.com 005 miranda CMDS=KNOCK,MAP,DCCALLOW,USERIP UHNAMES NAMESX SAFELIST HCN MAXCHANNELS=10 CHANLIMIT=#:10 MAXLIST=b:60,e:60,I:60 NICKLEN=30 CHANNELLEN=32 TOPICLEN=307 KICKLEN=307 AWAYLEN=307 :are supported by this server
        :irc.example.com 005 miranda MAXTARGETS=20 WALLCHOPS WATCH=128 WATCHOPTS=A SILENCE=15 MODES=12 CHANTYPES=# PREFIX=(ohv)@%+ CHANMODES=beIqa,kfL,lj,psmntirRcOAQKVCuzNSMTG NETWORK=Example CASEMAPPING=ascii EXTBAN=~,cqnr ELIST=MNUCT :are supported by this server
        :irc.example.com 005 miranda STATUSMSG=@%+ EXCEPTS INVEX :are supported by this server
        :irc.example.com 251 miranda :There are 16 users and 105 invisible on 2 servers
        :irc.example.com 252 miranda 14 :operator(s) online
        :irc.example.com 254 miranda 40 :channels formed
        :irc.example.com 255 miranda :I have 104 clients and 1 servers
        :irc.example.com 265 miranda :Current Local Users: 104  Max: 500
        :irc.example.com 266 miranda :Current Global Users: 121  Max: 892
        :irc.example.com 375 miranda :- irc.example.com Message of the Day -
        :irc.example.com 372 miranda :-            _                     _
        :irc.example.com 372 miranda :-           (_)                   | |
        :irc.example.com 372 miranda :-  _ __ ___  _ _ __ __ _ _ __   __| | __ _
        :irc.example.com 372 miranda :- | '_ ` _ \| | '__/ _` | '_ \ / _` |/ _` |
        :irc.example.com 372 miranda :- | | | | | | | | | (_| | | | | (_| | (_| |
        :irc.example.com 372 miranda :- |_| |_| |_|_|_|  \__,_|_| |_|\__,_|\__,_|
        :irc.example.com 372 miranda :-
        :irc.example.com 372 miranda :-   https://github.com/borntyping/miranda
        :irc.example.com 372 miranda :-        http://www.borntyping.co.uk
        :irc.example.com 372 miranda :-
        :irc.example.com 376 miranda :End of /MOTD command.
        :miranda MODE miranda :+i
        :miranda!miranda@example.com JOIN :#test
        :elara.ivixor.net 353 test = #test :@test
        :elara.ivixor.net 366 test #test :End of /NAMES list.
        :sam!sam@example.com JOIN :#test
        :sam!sam@example.com PRIVMSG #test :hello
        :sam!sam@example.com PART #test
        :miranda!miranda@example.com PART #test
        ERROR :Closing Link: miranda[0.0.0.0] (Quit: miranda)"""

    def read(self):
        lines = self.stream.split('\n')
        for line in lines[1:]:
            if line.startswith('ERROR'):
                raise StopIteration
            event = IRCEvent.parse(line.strip(), connection=self)
            if event is not None:
                yield event

def test_irc():
    connection = MockIRC()
    assert [event for event in connection.read()]
    assert connection.motd
