from __future__ import absolute_import, print_function, unicode_literals

import py.test

from miranda.events import Message
from miranda.irc.events import IRCEvent, Join, Part, Quit, Privmsg

class MockConnection(object):
    nick = 'miranda'

def line(line):
    return IRCEvent.parse(line, connection=MockConnection())

@py.test.fixture
def privmsg():
    return line(':sender!test@test PRIVMSG miranda :Hello')

@py.test.fixture
def chanmsg():
    return line(':sender!test@test PRIVMSG #channel :Hello')

@py.test.mark.parametrize(('command', 'type'), [
    ('JOIN', Join),
    ('PART', Part),
    ('QUIT', Quit),
])
def test_type(command, type):
    assert isinstance(line(':test!test@test '+command+' #test'), type)

def test_private_message_class(privmsg):
    assert isinstance(privmsg, Privmsg)
    assert isinstance(privmsg, Message)

def test_private_message_channel(privmsg):
    assert privmsg.private == True
    assert privmsg.channel == 'sender'

def test_channel_message_class(chanmsg):
    assert isinstance(chanmsg, Privmsg)
    assert isinstance(chanmsg, Message)

def test_channel_message_channel(chanmsg):
    assert chanmsg.private == False
    assert chanmsg.channel == '#channel'
