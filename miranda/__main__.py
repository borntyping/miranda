"""
Run miranda as a Python module

python -m miranda <opts...>
"""

from __future__ import absolute_import, print_function, unicode_literals

from miranda import main

if __name__ == '__main__':
    main()
