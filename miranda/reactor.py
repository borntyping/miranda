"""The base EventHandler class and the Reactor class"""

from __future__ import absolute_import, print_function, unicode_literals

import select
import traceback

from miranda.exceptions import Disconnected, PermanentlyDisconnected
from miranda.utils import quick_repr, tree

class EventHandler(object):
    fd = None

    def __enter__(self):
        self.connect()

    def __exit__(self, exc_type, exc_value, traceback):
        self.disconnect(exc_value)

    def connect(self):
        raise NotImplementedError

    def read(self):
        raise NotImplementedError

    def write(self):
        raise NotImplementedError

    def reconnect(self, exception=None):
        self.disconnect(exception)
        self.connect()

    def disconnect(self, exception=None):
        raise NotImplementedError

class Reactor(object):
    _timeout = 1000 * 60
    _input_mask = select.POLLPRI | select.POLLIN
    _error_mask = select.POLLERR | select.POLLHUP | select.POLLNVAL
    
    def __init__(self, connections, dispatch=None):
        self.connections = connections
        self.dispatch = dispatch or self._default_dispatch

        self._poll = select.poll()
        self._descriptors = dict()

    def __enter__(self):
        """Connect to each connection"""
        for connection in self.connections:
            self.connect(connection)

    def __exit__(self, *error):
        """Disconnect from each connection"""
        for handler in list(self._descriptors.values()):
            self.disconnect(handler)

    def __repr__(self):
        return quick_repr(self, attributes={
            'connections': len(self.connections),
        })

    def __str__(self):
        return "{0} instance with {1} connections".format(
            self.__class__.__name__, len(self.connections))

    def __children__(self):
        return self.connections

    def _default_dispatch(self, *args):
        print(*args)

    def connect(self, handler):
        try:
            handler.connect()
        except Exception as exception:
            print("Handler {handler} failed to connect:\n  {message}".format(
                handler=handler, message=exception))
            print(traceback.format_exc(exception), end='')
        else:
            self._descriptors[handler.fd] = handler
            self._poll.register(handler.fd, self._input_mask)

    def disconnect(self, handler, exception=None):
        """Unregister a connection and tell it to disconnect"""
        self._poll.unregister(handler.fd)
        self._descriptors.pop(handler.fd)
        handler.disconnect(exception)

    def go(self):
        """Start the context manager, loop until there a no connections left,
        and handle user interrupts"""
        with self:
            while self._descriptors:
                try:
                    self.tick()
                except KeyboardInterrupt:
                    print("\nShutting down...")
                    break
        return self

    def tick(self):
        """Poll for events and read events from the event handler"""
        for fd, event in self._poll.poll(self._timeout):
            handler = self._descriptors[fd]
            if event & self._error_mask:
                self.disconnect(handler)
            elif event & self._input_mask:
                self.tick_handler(handler)

    def tick_handler(self, handler):
        """Read events from a connection and dispatch them"""
        try:
            try:
                for event in handler.read():
                    self.dispatch(event)
            except Disconnected as exception:
                handler.reconnect(exception)
        except PermanentlyDisconnected as exception:
            self.disconnect(handler, exception)
            print("Permanently disconnected from {0}:\n\t{1!s}".format(
                handler, exception))
