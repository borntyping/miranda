from __future__ import absolute_import, print_function, unicode_literals

from miranda.console import ConsoleInput
from miranda.events import NullEvent
from miranda.exceptions import PermanentlyDisconnected
from miranda.irc import IRCEvent
from miranda import listener, Namespace

# from miranda.events.generic import NullEvent, Message

@listener(NullEvent)
def null_listener(event):
    pass

irc = Namespace()

@irc.listener(IRCEvent)
def irc_listener(event):
    print(event.text)

console = Namespace()

@console.listener(ConsoleInput)
def console_listener(event):
    if event.text == 'exit':
        raise PermanentlyDisconnected
    event.console.write("Received console input: '{0}'".format(event.text))
