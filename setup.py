#!/usr/bin/env python

from __future__ import absolute_import, print_function, unicode_literals

from sys import version_info

from setuptools import setup, find_packages

setup(
    name             = "miranda",
    version          = "0.6.0",

    author           = "Sam Clements",
    author_email     = "sam@borntyping.co.uk",
    url              = "https://github.com/borntyping/miranda",

    description      = "A modern IRC bot",
    long_description = open('README.rst').read(),

    packages         = find_packages(),

    install_requires = ['argparse', 'six'],

    entry_points     = {
        'console_scripts': [
            'miranda = miranda:main',
            'miranda{0}.{1} = miranda:main'.format(*version_info),
        ]
    },

    classifiers      = [
        'Development Status :: 4 - Beta',
        'Environment :: Console',
        'License :: OSI Approved',
        'License :: OSI Approved :: MIT License',
        'Operating System :: Unix',
        'Programming Language :: Python',
        'Programming Language :: Python :: 2',
        'Programming Language :: Python :: 2.6',
        'Programming Language :: Python :: 2.7',
        'Programming Language :: Python :: 3',
        'Programming Language :: Python :: 3.2',
        'Programming Language :: Python :: 3.3',
        'Topic :: Communications',
        'Topic :: Communications :: Chat',
        'Topic :: Communications :: Chat :: Internet Relay Chat',
    ],
)
