=======
Miranda
=======

.. image:: https://pypip.in/v/miranda/badge.png
    :target: https://pypi.python.org/pypi/miranda
    :alt: Latest PyPI version

.. image:: https://travis-ci.org/borntyping/miranda.png
    :target: https://travis-ci.org/borntyping/miranda
    :alt: Travis-CI build status

Miranda is an IRC bot, currently in development. It is based on `Aleph`_ - an older project of mine - and takes ideas from `saxo`_.

.. _Aleph: https://github.com/borntyping/aleph
.. _saxo: https://github.com/sbp/saxo

Aims
----

In order of priority:

* ✓ Class based event system
* ✗ Script-based commands
* ✗ An interactive console
* ✓ Unified codebase for Python 2 and 3
* ✓ Event handlers as context managers, so they can be used individually

MIT Licence
----------

::

    Copyright (c) 2013 Sam Clements <sam@borntyping.co.uk>

    Permission is hereby granted, free of charge, to any person obtaining a copy
    of this software and associated documentation files (the "Software"), to deal
    in the Software without restriction, including without limitation the rights
    to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
    copies of the Software, and to permit persons to whom the Software is
    furnished to do so, subject to the following conditions:

    The above copyright notice and this permission notice shall be included in
    all copies or substantial portions of the Software.

    THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
    IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
    FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
    AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
    LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
    OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
    THE SOFTWARE.